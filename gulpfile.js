"use strict";

var gulp = require('gulp');
var sass = require('gulp-sass');
var path = require('path');
var inlineCss = require('gulp-inline-css');
var autoprefixer = require('gulp-autoprefixer');
var cssbeautify = require('gulp-cssbeautify');
var replace = require('gulp-replace');

gulp.task('css', function () {
    gulp.src('scss/style.scss')
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(cssbeautify())
        .pipe(gulp.dest('./'))
        .pipe(gulp.dest('./build/'));
});

gulp.task('watch', function () {
    gulp.watch('scss/style.scss', ['css']);
});

gulp.task('inliner', function () {
    return gulp.src('build/**/*.html')
        .pipe(inlineCss())
        .pipe(replace('image/', 'http://compton.bh.net.ua/image/')) // replace server path
        .pipe(replace('</title>\n    \n', 
            '</title>\n    ' +
            '<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">\n    ' +
            '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">\n    ' +
            '<meta name="viewport" content="width=device-width">'))
        .pipe(gulp.dest('dest/'));
});

// default
gulp.task('default', ['watch']);